Name:           narayana
Version:        5.3.3
Release:        6
Summary:        Distributed Transaction Manager
License:        LGPLv2+
URL:            http://narayana.io/
Source0:        https://github.com/jbosstm/narayana/archive/%{version}.Final/%{name}-%{version}.Final.tar.gz

BuildRequires:  maven-local mvn(commons-httpclient:commons-httpclient)
BuildRequires:  mvn(dom4j:dom4j) mvn(java_cup:java_cup) mvn(javax.annotation:javax.annotation-api)
BuildRequires:  mvn(javax.enterprise:cdi-api) mvn(junit:junit)
BuildRequires:  mvn(org.apache.activemq:artemis-journal)
BuildRequires:  mvn(org.apache.ant:ant) mvn(org.apache.ant:ant-junit)
BuildRequires:  mvn(org.apache.felix:maven-bundle-plugin)
BuildRequires:  mvn(org.apache.maven.plugins:maven-antrun-plugin)
BuildRequires:  mvn(org.apache.maven.plugins:maven-clean-plugin)
BuildRequires:  mvn(org.apache.maven.plugins:maven-dependency-plugin)
BuildRequires:  mvn(org.apache.maven.plugins:maven-enforcer-plugin)
BuildRequires:  mvn(org.apache.maven.plugins:maven-shade-plugin)
BuildRequires:  mvn(org.apache.maven.plugins:maven-source-plugin)
BuildRequires:  mvn(org.apache.maven.plugins:maven-war-plugin)
BuildRequires:  mvn(org.codehaus.mojo:build-helper-maven-plugin)
BuildRequires:  mvn(org.codehaus.mojo:idlj-maven-plugin)
BuildRequires:  mvn(org.eclipse.osgi:org.eclipse.osgi) mvn(org.eclipse.osgi:org.eclipse.osgi.services)
BuildRequires:  mvn(org.jacoco:org.jacoco.ant) mvn(org.jacorb:jacorb)
BuildRequires:  mvn(org.jacorb:jacorb-idl-compiler) mvn(org.jboss:jboss-parent:pom:)
BuildRequires:  mvn(org.jboss:jboss-transaction-spi) >= 7.3.0
BuildRequires:  mvn(org.jboss.byteman:byteman) mvn(org.jboss.byteman:byteman-dtest)
BuildRequires:  mvn(org.jboss.byteman:byteman-submit) mvn(org.jboss.integration:jboss-corba-ots-spi)
BuildRequires:  mvn(org.jboss.ironjacamar:ironjacamar-spec-api)
BuildRequires:  mvn(org.jboss.jandex:jandex-maven-plugin)
BuildRequires:  mvn(org.jboss.logging:jboss-logging) mvn(org.jboss.logging:jboss-logging-processor)
BuildRequires:  mvn(org.jboss.resteasy:resteasy-jaxb-provider)
BuildRequires:  mvn(org.jboss.resteasy:resteasy-jaxrs) mvn(org.jboss.resteasy:resteasy-jettison-provider)
BuildRequires:  mvn(org.jboss.resteasy:tjws) mvn(org.jboss.spec.javax.ejb:jboss-ejb-api_3.1_spec)
BuildRequires:  mvn(org.jboss.spec.javax.interceptor:jboss-interceptors-api_1.2_spec)
BuildRequires:  mvn(org.jboss.spec.javax.jms:jboss-jms-api_1.1_spec)
BuildRequires:  mvn(org.jboss.spec.javax.servlet:jboss-servlet-api_3.0_spec)
BuildRequires:  mvn(org.jboss.spec.javax.transaction:jboss-transaction-api_1.2_spec)
BuildRequires:  mvn(org.jboss.ws:jbossws-api) mvn(org.springframework:spring-tx)
BuildRequires:  mvn(tanukisoft:wrapper)
BuildRequires:  byteman-bmunit

BuildRequires:       mvn(org.eclipse.aether:aether-connector-basic)
BuildRequires:       mvn(org.eclipse.aether:aether-transport-wagon)
BuildRequires:       mvn(org.apache.maven.wagon:wagon-http)
BuildRequires:       mvn(org.apache.maven.wagon:wagon-provider-api)

BuildArch:      noarch

%description
A set of JBoss modules that fully support ACID transactions are distributed across multiple resource managers
and application servers. It implements a two-phase commit (2PC) server instance and CORBA OTS resource with a
supported distributed transaction manager (DTM) for XA resource manager JBoss.

%package        help
Summary:        Documentation for narayana
Provides:       %{name}-javadoc = %{version}-%{release}
Obsoletes:      %{name}-javadoc < %{version}-%{release}

%description    help
This package provides documentation for narayana.

%prep
%autosetup -n narayana-%{version}.Final -p1

find . -name "*.jar"   -type f -delete
find . -name "*.class" -type f -delete
find . -name "*.dll"   -type f -delete
find . -name "*.exe"   -type f -delete
find . -name "*.so"    -type f -delete

rm -r ArjunaJTS/services/bin/*

%pom_disable_module localjunit XTS
%pom_disable_module narayana-full
%pom_disable_module webservice rts/at

%pom_remove_plugin -r "org.codehaus.mojo:findbugs-maven-plugin"
%pom_remove_plugin -r "org.sonatype.plugins:nexus-staging-maven-plugin"
%pom_remove_plugin -r "org.jboss.byteman:byteman-rulecheck-maven-plugin"
%pom_remove_plugin -r :nexus-staging-maven-plugin ArjunaJTS/narayana-jts-jacorb  ArjunaJTS/narayana-jts-idlj
%pom_remove_plugin ":maven-dependency-plugin" txbridge

%pom_remove_dep "sun.jdk:jconsole" ArjunaCore/arjuna
%pom_remove_dep -r "orson:orson" ArjunaCore/arjuna ArjunaCore/arjunacore
%pom_remove_dep -r "org.jboss.arquillian.junit:arquillian-junit-container"
%pom_remove_dep -r "org.jboss.openjdk-orb:openjdk-orb"
%pom_remove_dep "org.jboss.osgi.metadata:jbosgi-metadata" osgi
%pom_remove_dep "org.jboss.osgi.metadata:jbosgi-metadata" osgi/jta

%pom_xpath_remove pom:Embed-Dependency osgi/jta
%pom_xpath_remove pom:Export-Package osgi/jta
%pom_xpath_remove pom:Private-Package osgi/jta
%pom_xpath_remove pom:Require-Capability osgi/jta

%pom_change_dep -r "jfree:jfreechart" "org.jfree:jfreechart" ArjunaCore/arjuna ArjunaCore/arjunacore
%pom_change_dep "org.osgi:org.osgi.core" "org.eclipse.osgi:org.eclipse.osgi" osgi
%pom_change_dep "org.osgi:org.osgi.core" "org.eclipse.osgi:org.eclipse.osgi" osgi/jta
%pom_change_dep "org.osgi:org.osgi.compendium" "org.eclipse.osgi:org.eclipse.osgi.services"  osgi
%pom_change_dep "org.osgi:org.osgi.compendium" "org.eclipse.osgi:org.eclipse.osgi.services"  osgi/jta

%pom_xpath_remove "pom:dependency[pom:type='war']" XTS/sar

for mod in orbportability jts; do
   %pom_xpath_inject "pom:plugin[pom:artifactId='maven-compiler-plugin']/pom:executions/pom:execution/pom:configuration" \
  "<useIncrementalCompilation>false</useIncrementalCompilation>" ArjunaJTS/${mod}
done

for mod in XTS/WS-C XTS/WS-T XTS/WSTX XTS/bridge; do
    %pom_xpath_inject "pom:plugin[pom:artifactId='maven-jar-plugin']/pom:executions" "
        <execution>
          <id>default-jar</id>
          <phase>skip</phase>
        </execution>" ${mod}
done
%pom_remove_dep org.jboss.narayana:common ArjunaJTS/orbportability
%pom_remove_dep -r org.jboss.resteasy:jaxrs-api rts/at/bridge rts/at/integration rts/at/tx rts/at/util

%pom_add_dep org.jboss.narayana:common:'${project.version}' ArjunaJTS/orbportability

%mvn_package :::api: __default
%mvn_package :::war: __noinstall

grep -rn '<artifactId>buildnumber-maven-plugin' | awk -F: '{print $1}' | for line in `xargs`;do sed -i '/<artifactId>buildnumber-maven-plugin/a\        <version>1.3<\/version>' $line;done

%build
%mvn_build -f -b

%install
%mvn_install

%files -f .mfiles
%doc README.md common/copyright.txt

%files help -f .mfiles-javadoc

%changelog
* Fri Aug 18 2023 Ge Wang <wang__ge@126.com> - 5.3.3-6
- Fix build failure due to buildnumber-maven-plugin updated

* Sun Dec 22 2019 fengbing <fengbing7@huawei.com> - 5.3.3-5
- Package init
